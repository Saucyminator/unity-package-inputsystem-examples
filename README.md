# Unity3D InputSystem examples
Visual examples of five different button interactions, import the sample scene to view them.

Buttons include:
- Hold
- MultiTap
- Press
- SlowTap
- Tap

[Video showing the interactions](https://www.youtube.com/watch?v=_rNJ-moe0zo)

## Versions tested

Name | Version
--- | ---
Unity version | 2019.3.14f1
InputSystem version | v1.0.0


## Installation
To install this package, follow the instructions in the [Package Manager documentation](https://docs.unity3d.com/Packages/com.unity.package-manager-ui@latest/index.html). 


## Changelog
[See CHANGELOG](CHANGELOG.md)


## License
[MIT License](LICENSE.md)
