﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

// NOTE: Input "canceled" isn't being used at all.

namespace Saucy.Examples {
  public class Press : Base {
    [SerializeField] private UnityEvent OnPerformed = new UnityEvent();
    [SerializeField] private UnityEvent OnReleased = new UnityEvent(); // Still operates under "Performed" input.

    private bool isPressing;

    public override void OnInput (InputAction.CallbackContext _context) {
      if (_context.performed) {
        isPressing = !isPressing;

        if (isPressing) {
          Debug.Log("Input PRESS: performed pressed");

          image.color = colorSuccess;

          OnPerformed?.Invoke();
        } else {
          Debug.Log("Input PRESS: performed released");

          image.color = colorNormal;

          OnReleased?.Invoke();
        }
      }
    }
  }
}
