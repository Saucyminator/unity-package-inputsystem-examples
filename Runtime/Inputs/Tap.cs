﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Saucy.Examples {
  public class Tap : Base {
    [SerializeField] private UIFader fader;
    [SerializeField] private float fadeSpeed = 2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet?).")] [SerializeField] private float maxTapDuration = 0.5f;

    [SerializeField] private UnityEvent OnStarted = new UnityEvent();
    [SerializeField] private UnityEvent OnPerformed = new UnityEvent();
    [SerializeField] private UnityEvent OnCanceled = new UnityEvent();

    private bool isPressing;
    private float currentTime;

    protected override void Awake () {
      base.Awake();

      fader = GetComponentInChildren<UIFader>();
    }

    private void Update () {
      if (isPressing) {
        currentTime += Time.deltaTime;
        image.fillAmount = Mathf.Clamp01(currentTime / maxTapDuration);
      }
    }

    public override void OnInput (InputAction.CallbackContext _context) {
      if (_context.started) {
        Debug.Log("Input TAP: started");

        StopAllCoroutines();
        isPressing = true;
        currentTime = 0f;
        image.fillAmount = 0f;
        image.color = colorNormal;

        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("Input TAP: performed");

        isPressing = false;
        image.color = colorSuccess;

        OnPerformed?.Invoke();

        StopAllCoroutines();
        StartCoroutine(TapFadeOut());
      }

      if (_context.canceled) {
        Debug.Log("Input TAP: canceled");

        isPressing = false;
        image.fillAmount = 1f;
        image.color = colorError;

        OnCanceled?.Invoke();

        StopAllCoroutines();
        StartCoroutine(TapFadeOut());
      }
    }

    private IEnumerator TapFadeOut () {
      fader.fadeSpeed = fadeSpeed;

      yield return fader.FadeOut();

      currentTime = 0f;
      image.fillAmount = 0f;
    }
  }
}
