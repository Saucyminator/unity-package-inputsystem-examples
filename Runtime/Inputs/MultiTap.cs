﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Saucy.Examples {
  public class MultiTap : Base {
    [SerializeField] private float resetTime = 0.2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet).")] [SerializeField] private int maxTaps = 2;

    [SerializeField] private UnityEvent OnStarted = new UnityEvent();
    [SerializeField] private UnityEvent OnPerformed = new UnityEvent();
    [SerializeField] private UnityEvent OnCanceled = new UnityEvent();

    private int currentTaps;

    protected override void Awake () {
      base.Awake();

      image.fillAmount = 0f;
      currentTaps = 0;
    }

    public override void OnInput (InputAction.CallbackContext _context) {
      if (_context.started) {
        Debug.Log("Input MULTITAP: started");

        ++currentTaps;
        image.fillAmount = Mathf.Clamp01(currentTaps / maxTaps);
        image.color = colorError;

        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("Input MULTITAP: performed");

        currentTaps = maxTaps;
        image.fillAmount = 1f;
        image.color = colorSuccess;

        OnPerformed?.Invoke();

        StopAllCoroutines();
        StartCoroutine(MultiTapReset(resetTime + 0.3f));
      }

      if (_context.canceled) {
        Debug.Log("Input MULTITAP: canceled");

        OnCanceled?.Invoke();

        StopAllCoroutines();
        StartCoroutine(MultiTapReset());
      }
    }

    private IEnumerator MultiTapReset (float _delay = 0f) {
      yield return new WaitForSeconds(_delay);

      currentTaps = 0;
      image.fillAmount = 0f;
      image.color = colorNormal;
    }
  }
}
