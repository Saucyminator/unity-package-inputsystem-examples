﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

// This class is used to fade in and out groups of UI elements. It contains a variety of functions for fading in different ways.

namespace Saucy.Examples {
  [RequireComponent(typeof(CanvasGroup))]
  public class UIFader : MonoBehaviour {
    public bool IsVisible { get; private set; } // Whether the UI elements are currently visible.
    [Tooltip("Should the UI elements be visible to start?")] public bool startVisible;
    [Tooltip("Should the UI elements begin fading with they start up? Fading can either be in or out (opposite of their starting alpha)")] public bool startWithFade;
    [Tooltip("The amount the alpha of the UI elements changes per second.")] public float fadeSpeed = 1f;
    [Tooltip("The amount in seconds before animating the fade.")] public float fadeOutDelay = 0f;

    [Tooltip("All the groups of UI elements that will fade in and out.")] [SerializeField] private CanvasGroup groupToFade;
    [SerializeField] private UnityEvent OnFadedIn = new UnityEvent();
    [SerializeField] private UnityEvent OnFadedOut = new UnityEvent();

    private void OnValidate () {
      if (groupToFade == null) {
        groupToFade = GetComponent<CanvasGroup>();
      }
    }

    private void OnEnable () {
      // If the object should start visible, set it to be visible. Otherwise, set it invisible
      if (startVisible) {
        SetVisible();
      } else {
        SetInvisible();
      }

      // If there shouldn't be any initial fade, leave this method
      if (!startWithFade) {
        return;
      }

      // If the object is currently visible, fade out. Otherwise fade in
      if (IsVisible) {
        StartFadeOut();
      } else {
        StartFadeIn();
      }
    }

    public void Flash () {
      StopAllCoroutines();
      StartCoroutine(ProcessFlash());
    }

    // Publicly accessible methods for fading in or fading out without needing to start a coroutine. These are needed in order for UI events (like buttons) to start a fade in or out.
    public void StartFadeIn () {
      StopAllCoroutines();
      StartCoroutine(FadeIn());
    }

    public void StartFadeIn (float _fadeSpeed, float _fadeOutDelay) {
      fadeSpeed = _fadeSpeed;
      fadeOutDelay = _fadeOutDelay;

      StopAllCoroutines();
      StartCoroutine(FadeIn());
    }

    public void StartFadeOut () {
      StopAllCoroutines();
      StartCoroutine(FadeOut());
    }

    public void StartFadeOut (float _fadeSpeed, float _fadeOutDelay) {
      fadeSpeed = _fadeSpeed;
      fadeOutDelay = _fadeOutDelay;

      StopAllCoroutines();
      StartCoroutine(FadeOut());
    }

    // These functions are used if fades are required to be instant.
    public void SetVisible () {
      groupToFade.alpha = 1f;
      IsVisible = true;
    }

    public void SetInvisible () {
      groupToFade.alpha = 0f;
      IsVisible = false;
    }

    public IEnumerator FadeIn () {
      // Fading needs to continue until the group is completely faded in
      while (groupToFade.alpha < 1f) {
        groupToFade.alpha += fadeSpeed * Time.deltaTime;

        yield return null;
      }

      // Set the alpha to max (can happen it doesn't set the alpha because of using a float)
      groupToFade.alpha = 1f;

      // Since everthing has faded in now, it is visible.
      IsVisible = true;

      OnFadedIn?.Invoke();
    }

    public IEnumerator FadeOut () {
      yield return new WaitForSeconds(fadeOutDelay);

      while (groupToFade.alpha > 0f) {
        groupToFade.alpha -= fadeSpeed * Time.deltaTime;

        yield return null;
      }

      groupToFade.alpha = 0f;

      IsVisible = false;

      OnFadedOut?.Invoke();
    }

    private IEnumerator ProcessFlash () {
      yield return StartCoroutine(FadeIn());
      yield return StartCoroutine(FadeOut());
    }
  }
}
