// GENERATED AUTOMATICALLY FROM 'Packages/com.saucy.unity-examples-inputsystem/Samples/Example/Inputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Saucy.Examples
{
    public class @Inputs : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @Inputs()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Inputs"",
    ""maps"": [
        {
            ""name"": ""UI"",
            ""id"": ""6846ba26-d75f-407c-a12f-56788effcf05"",
            ""actions"": [
                {
                    ""name"": ""Hold"",
                    ""type"": ""Button"",
                    ""id"": ""59068713-b677-4920-8458-b1ca020856de"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=1)""
                },
                {
                    ""name"": ""MultiTap"",
                    ""type"": ""Button"",
                    ""id"": ""a894b08c-f67e-4f59-a7d3-f8ca7186db63"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""MultiTap""
                },
                {
                    ""name"": ""Press"",
                    ""type"": ""Button"",
                    ""id"": ""be16d2c6-cb08-4adf-9995-f30399cd916d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""SlowTap"",
                    ""type"": ""Button"",
                    ""id"": ""581d7f0b-a949-44e1-bc70-d99d1440448e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""SlowTap(duration=1)""
                },
                {
                    ""name"": ""Tap"",
                    ""type"": ""Button"",
                    ""id"": ""85e37b6a-3fdf-4595-949f-4ce6c4f6ff8f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap(duration=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""37e48178-7902-41e4-8a3c-efc5736c5258"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Hold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6f81a7dc-6c6e-43d8-9ece-75b7d75312b2"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1cd1d14b-ad0b-4133-9978-71c5f156722c"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""MultiTap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9905ab84-8164-48db-86f9-08f60a68502d"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MultiTap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""931cdef1-14a9-4a98-9de6-a9d795dd8a63"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Press"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4df05109-09b4-499e-a739-bb0b718cc846"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Press"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0b2df1a0-ea68-43a2-8af4-a24a7eb48487"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""SlowTap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5263ee92-6a8b-46b8-a169-f139c3a0aae7"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SlowTap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""38a46a8e-bc46-45f7-9b4a-0822c0f75d7e"",
                    ""path"": ""<Keyboard>/5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard & Mouse"",
                    ""action"": ""Tap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5c01f0ae-4de0-4778-ab9d-b625d8911d3a"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Tap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard & Mouse"",
            ""bindingGroup"": ""Keyboard & Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Touch"",
            ""bindingGroup"": ""Touch"",
            ""devices"": [
                {
                    ""devicePath"": ""<Touchscreen>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Joystick"",
            ""bindingGroup"": ""Joystick"",
            ""devices"": [
                {
                    ""devicePath"": ""<Joystick>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""XR"",
            ""bindingGroup"": ""XR"",
            ""devices"": [
                {
                    ""devicePath"": ""<XRController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // UI
            m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
            m_UI_Hold = m_UI.FindAction("Hold", throwIfNotFound: true);
            m_UI_MultiTap = m_UI.FindAction("MultiTap", throwIfNotFound: true);
            m_UI_Press = m_UI.FindAction("Press", throwIfNotFound: true);
            m_UI_SlowTap = m_UI.FindAction("SlowTap", throwIfNotFound: true);
            m_UI_Tap = m_UI.FindAction("Tap", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // UI
        private readonly InputActionMap m_UI;
        private IUIActions m_UIActionsCallbackInterface;
        private readonly InputAction m_UI_Hold;
        private readonly InputAction m_UI_MultiTap;
        private readonly InputAction m_UI_Press;
        private readonly InputAction m_UI_SlowTap;
        private readonly InputAction m_UI_Tap;
        public struct UIActions
        {
            private @Inputs m_Wrapper;
            public UIActions(@Inputs wrapper) { m_Wrapper = wrapper; }
            public InputAction @Hold => m_Wrapper.m_UI_Hold;
            public InputAction @MultiTap => m_Wrapper.m_UI_MultiTap;
            public InputAction @Press => m_Wrapper.m_UI_Press;
            public InputAction @SlowTap => m_Wrapper.m_UI_SlowTap;
            public InputAction @Tap => m_Wrapper.m_UI_Tap;
            public InputActionMap Get() { return m_Wrapper.m_UI; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
            public void SetCallbacks(IUIActions instance)
            {
                if (m_Wrapper.m_UIActionsCallbackInterface != null)
                {
                    @Hold.started -= m_Wrapper.m_UIActionsCallbackInterface.OnHold;
                    @Hold.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnHold;
                    @Hold.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnHold;
                    @MultiTap.started -= m_Wrapper.m_UIActionsCallbackInterface.OnMultiTap;
                    @MultiTap.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnMultiTap;
                    @MultiTap.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnMultiTap;
                    @Press.started -= m_Wrapper.m_UIActionsCallbackInterface.OnPress;
                    @Press.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnPress;
                    @Press.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnPress;
                    @SlowTap.started -= m_Wrapper.m_UIActionsCallbackInterface.OnSlowTap;
                    @SlowTap.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnSlowTap;
                    @SlowTap.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnSlowTap;
                    @Tap.started -= m_Wrapper.m_UIActionsCallbackInterface.OnTap;
                    @Tap.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnTap;
                    @Tap.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnTap;
                }
                m_Wrapper.m_UIActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Hold.started += instance.OnHold;
                    @Hold.performed += instance.OnHold;
                    @Hold.canceled += instance.OnHold;
                    @MultiTap.started += instance.OnMultiTap;
                    @MultiTap.performed += instance.OnMultiTap;
                    @MultiTap.canceled += instance.OnMultiTap;
                    @Press.started += instance.OnPress;
                    @Press.performed += instance.OnPress;
                    @Press.canceled += instance.OnPress;
                    @SlowTap.started += instance.OnSlowTap;
                    @SlowTap.performed += instance.OnSlowTap;
                    @SlowTap.canceled += instance.OnSlowTap;
                    @Tap.started += instance.OnTap;
                    @Tap.performed += instance.OnTap;
                    @Tap.canceled += instance.OnTap;
                }
            }
        }
        public UIActions @UI => new UIActions(this);
        private int m_KeyboardMouseSchemeIndex = -1;
        public InputControlScheme KeyboardMouseScheme
        {
            get
            {
                if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard & Mouse");
                return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
            }
        }
        private int m_GamepadSchemeIndex = -1;
        public InputControlScheme GamepadScheme
        {
            get
            {
                if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
                return asset.controlSchemes[m_GamepadSchemeIndex];
            }
        }
        private int m_TouchSchemeIndex = -1;
        public InputControlScheme TouchScheme
        {
            get
            {
                if (m_TouchSchemeIndex == -1) m_TouchSchemeIndex = asset.FindControlSchemeIndex("Touch");
                return asset.controlSchemes[m_TouchSchemeIndex];
            }
        }
        private int m_JoystickSchemeIndex = -1;
        public InputControlScheme JoystickScheme
        {
            get
            {
                if (m_JoystickSchemeIndex == -1) m_JoystickSchemeIndex = asset.FindControlSchemeIndex("Joystick");
                return asset.controlSchemes[m_JoystickSchemeIndex];
            }
        }
        private int m_XRSchemeIndex = -1;
        public InputControlScheme XRScheme
        {
            get
            {
                if (m_XRSchemeIndex == -1) m_XRSchemeIndex = asset.FindControlSchemeIndex("XR");
                return asset.controlSchemes[m_XRSchemeIndex];
            }
        }
        public interface IUIActions
        {
            void OnHold(InputAction.CallbackContext context);
            void OnMultiTap(InputAction.CallbackContext context);
            void OnPress(InputAction.CallbackContext context);
            void OnSlowTap(InputAction.CallbackContext context);
            void OnTap(InputAction.CallbackContext context);
        }
    }
}
