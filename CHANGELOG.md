# Changelog

## v1.0.0 (2020-05-25)
- Fixed: Youtube video link in README and Documentation files.


## v0.3.0 (2020-05-25)
- Updated: README and Documentation files with updated info.
- Removed: Video from project, now a youtube video instead.


## v0.2.0 (2020-05-25)
- Removed: Text UI now uses Unity UI instead of TextMeshPro.
- Updated: Tweaked background color a bit.
- Fixed: Added PlayerInput component to the UI Canvas and now uses the UnityEvents to call the button-scripts.
- Cleanup: Moved everything under the same namespace.
- Updated: Remade everything as a Unity Package instead of a unity project (removed unnecessary files, restructured all folders, etc).


## v0.1.0 (2019-12-26)
- Added: Five different button interactions: Hold, MultiTap, Press (& release), SlowTap, and Tap.
